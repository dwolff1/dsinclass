/**********************************************
* File: AdjListVecSol.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Contains an STL implementation of an Adjacency List
* graph using C++ STL Vector 
*
* g++ -g -std=c++11 -Wpedantic AdjListVecSol.cpp -o AdjListVecSol
* ./AdjListVecSol
* Number of buckets is: 6
* Origin: {Destination, Weight}
* N: {N O 1} {N T 1}
* O: {O N 1} {O R 1} {O E 1} 
* T: {T N 1} {T R 1}
* R: {R O 1} {R T 1} {R D 1}
* E: {E O 1} {E D 1}
* D: {D R 1} {D E 1}
*
* Number of buckets is: 5
* Origin: {Destination, Weight}
* 0: {0 1 1} {0 4 1}
* 1: {1 0 1} {1 4 1} {1 2 1} {1 3 1}
* 4: {4 0 1} {4 1 1} {4 3 1}
* 2: {2 1 1} {2 3 1}
* 3: {3 1 1} {3 2 1} {3 4 1}
* 
**********************************************/

#include <iostream>
#include <vector>
//#include <iterator>
#include <unordered_map>
//#include <algorithm>
#include <fstream>

// data structure to store graph edges
template<class T>
struct Edge {
	T src;
	T dest;
	int weight;
	
   template <typename U>      // all instantiations of this template are friends
   friend std::ostream& operator<<( std::ostream&, const Edge<U>& );

};

template <typename T>
std::ostream& operator<<( std::ostream& os, const Edge<T>& theEdge) {
   
   std::cout << "{" << theEdge.src << " " << theEdge.dest << " " << theEdge.weight << "} ";
   
   return os;
}


template<class T>
struct Vertex {
	T value;
	mutable std::vector< Edge <T> > edges;
	
	bool operator==(const Vertex<T>& rhs) const{
		
		return value == rhs.value;
		
	}
	
	bool operator<(const Vertex<T>& rhs) const{
		
		return value < rhs.value; 
		
	}
};

// class to represent a graph object
template<class T>
class Graph
{
	public:
		// construct a vectors to represent an adjacency list
		std::vector< Vertex<T> > adjList;
		
		// Hash Table to correlate Verticies with location in vector
		std::unordered_map< T, int > hashVerts;

		/********************************************
		* Function Name  : Graph
		* Pre-conditions : const std::vector< Edge<T> > &edges
		* Post-conditions: none
		*  
		* Graph Constructor
		********************************************/
		Graph(std::vector< Edge<T> > &edges)
		{

			// add edges to the directed graph
			for (auto &edge: edges)
			{

				// Insert Origin 
				addEdge(edge);
				
				// Since this is undirected, put in opposite
				// Uncomment if graph is directional
				// Create a temp edge to reverse origin and destination 
				Edge<T> tempEdge = {edge.dest, edge.src, edge.weight};
				addEdge(tempEdge);
				
			}
		}
		
		/********************************************
		* Function Name  : addEdge
		* Pre-conditions : Edge<T> edge
		* Post-conditions: none
		*  
		********************************************/
		void addEdge(const Edge<T>& edge){
			
			// Create a temporary vertex with the source
			Vertex<T> tempVert;
			tempVert.value = edge.src;
			
			// Element was not found
			if(hashVerts.count(tempVert.value) == 0){
				
				// HashWords.insert( {wordIn, 1} );
				hashVerts.insert( {tempVert.value, adjList.size()} );
				
				// Inset the edge into the temporary vertex 
				// Serves as the first edge 
				tempVert.edges.push_back(edge);
				
				// Insert the vertex into the set 
				adjList.push_back(tempVert);
				
			}
			// Element was found!
			else{
				
				// Use the hash to get the location in adjList, then push onto the edges vector 
				adjList.at(hashVerts[tempVert.value]).edges.push_back(edge);
				
			}
		}
};


/********************************************
* Function Name  : printGraph
* Pre-conditions : const Graph<T>& graph
* Post-conditions: none
*  
* Prints all the elements in the graph 
********************************************/
template<typename T>
void printGraph(const Graph<T>& graph){
	
	std::cout << "Number of buckets is: " << graph.adjList.size() << std::endl;
	std::cout << "Origin: {Destination, Weight}" << std::endl;

	for(int iter = 0; iter < graph.adjList.size(); iter++){
		
		std::cout << graph.adjList.at(iter).value << ": ";
		
		for(int jter = 0; jter < graph.adjList.at(iter).edges.size(); jter++){
			
			std::cout << graph.adjList.at(iter).edges.at(jter);
			
		}
		
		std::cout << std::endl;
	}
	
	std::cout << std::endl;
	
}


/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* Main Driver Function 
********************************************/
int main(int argc, char** argv)
{

	// Allocate the edges as shown in the description
	std::vector< Edge<char> > edges =
	{
		{'N', 'O', 1}, {'N', 'T', 1}, {'O', 'R', 1}, 
		{'O', 'E', 1}, {'T', 'R', 1}, {'R', 'D', 1}, 
		{'E', 'D', 1} 
	};

	// construct graph
	Graph<char> charGraph(edges);

	// print adjacency list representation of graph
	printGraph(charGraph);
	
	
	// Allocate the edges as shown in the description
	std::vector< Edge<int> > edgeInts =
	{
		{0, 1, 1}, {0, 4, 1}, 
		{1, 4, 1}, {1, 2, 1}, {1, 3, 1}, 
		{2, 3, 1}, 
		{3, 4, 1} 
	};
	
	// construct graph
	Graph<int> intGraph(edgeInts);

	// print adjacency list representation of graph
	printGraph(intGraph);

	return 0;
}