/**********************************************
 * File: AdjListVec2.cpp
 * Author: Matthew Morrison
 * Email: matt.morrison@nd.edu
 *
 * Contains an STL implementation of an Adjacency List
 * graph (no directions or weights) using C++ STL Vector
 *
 * mmorri22@remote305:~/CSE21312/InClass/GraphSTL$ g++ -g -std=c++11 -Wpedantic AdjListVec.cpp -o AdjListVec
 * mmorri22@remote305:~/CSE21312/InClass/GraphSTL$ ./AdjListVec
 * Number of buckets is: 6
 * Origin: {Destination, Weight}
 * N: {O, 1} {T, 1}
 * O: {N, 1} {R, 1} {E, 1}
 * T: {N, 1} {R, 1}
 * R: {O, 1} {T, 1} {D, 1}
 * E: {O, 1} {D, 1}
 * D: {R, 1} {E, 1}
 *
 * Number of buckets is: 5
 * Origin: {Destination, Weight}
 * 0: {1, 1} {4, 1}
 * 1: {0, 1} {4, 1} {2, 1} {3, 1}
 * 4: {0, 1} {1, 1} {3, 1}
 * 2: {1, 1} {3, 1}
 * 3: {1, 1} {2, 1} {4, 1}
 *
 **********************************************/

#include <iostream>
#include <vector>
//#include <iterator>
#include <unordered_map>
//#include <algorithm>
#include <fstream>

// In Class Code will go here
template<class T>
struct Edge{
    T src, dest;
    int weight;
    
    template<class U>
    friend std::ostream& operator<<(std::ostream& os, const Edge<U>&);
};

template<class T>
std::ostream& operator<<(std::ostream& os, const Edge<T>& theEdge) {
    
    os << "(" <<theEdge.src << " " <<theEdge.dest << theEdge.dest << " ";
    os << theEdge.weight <<") " << std::endl;
    
    return os;
}

template<class T>
struct Vertex{
    
    T value;
    mutable std::vector<Edge<T> > edges;
    bool operator==(const std::vector<T>& rhs) const{
        return value==rhs.value;
    }
    
    bool operator<(const Vertex<T>& rhs)const{
        return value<rhs.value;
    }
    
};

template<class T>
class Graph{
    
public:
    //Constructs a vector of graph verticies
    std::vector<Vertex<T> > adjList;
    
    //Hash Table to correlate vertex with vector loction
    std::unordered_map<T,int>hashVerts;
    Graph(std::vector<Edge<T> >& edges){
        for (auto &edge:edges){
            //Add the edge
            addEdge(edge);
            //Add Reverse Edge
            Edge<T> tempEdge = {edge.dest, edge.src, edge.weight};
            addEdge(tempEdge);
        }
	}
        void addEdge(const Edge <T> edge){
            Vertex<T> tempVert;
            tempVert.value = edge.src;
            
            if(hashVerts.count(tempVert.value)==0){
                hashVerts.insert({tempVert.value,adjList.size() });
                tempVert.edges.push_back(edge);
                adjList.push_back(tempVert);
            }
            else{
                adjList.at(hashVerts[tempVert.value]).edges.push_back(edge);
            }
        }
};

/********************************************
 * Function Name  : main
 * Pre-conditions : int argc, char** argv
 * Post-conditions: int
 *
 * Main Driver Function
 ********************************************/
int main(int argc, char** argv)
{
    
    // Allocate the edges as shown in the description
    std::vector< Edge<char> > edges =
    {
        {'N', 'O', 1}, {'N', 'T', 1}, {'O', 'R', 1},
        {'O', 'E', 1}, {'T', 'R', 1}, {'R', 'D', 1},
        {'E', 'D', 1}
    };
    
    // construct graph
    //Graph<char> charGraph(edges);
    
    // print adjacency list representation of graph
    //printGraph(charGraph);
    
    
    // Allocate the edges as shown in the description
    std::vector< Edge<int> > edgeInts =
    {
        {0, 1, 1}, {0, 4, 1},
        {1, 4, 1}, {1, 2, 1}, {1, 3, 1},
        {2, 3, 1},
        {3, 4, 1}
    };
    
    // construct graph
    //Graph<int> intGraph(edgeInts);
    
    // print adjacency list representation of graph
    //printGraph(intGraph);
    
    return 0;
}



