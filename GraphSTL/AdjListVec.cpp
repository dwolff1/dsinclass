/**********************************************
* File: AdjListVec.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Contains an STL implementation of an Adjacency List
* graph (no directions or weights) using C++ STL Vector 
*
* mmorri22@remote305:~/CSE21312/InClass/GraphSTL$ g++ -g -std=c++11 -Wpedantic AdjListVec.cpp -o AdjListVec
* mmorri22@remote305:~/CSE21312/InClass/GraphSTL$ ./AdjListVec
* Number of buckets is: 6
* Origin: {Destination, Weight}
* N: {O, 1} {T, 1}
* O: {N, 1} {R, 1} {E, 1}
* T: {N, 1} {R, 1}
* R: {O, 1} {T, 1} {D, 1}
* E: {O, 1} {D, 1}
* D: {R, 1} {E, 1}
* 
* Number of buckets is: 5
* Origin: {Destination, Weight}
* 0: {1, 1} {4, 1}
* 1: {0, 1} {4, 1} {2, 1} {3, 1}
* 4: {0, 1} {1, 1} {3, 1}
* 2: {1, 1} {3, 1}
* 3: {1, 1} {2, 1} {4, 1}
* 
**********************************************/

#include <iostream>
#include <vector>
//#include <iterator>
#include <unordered_map>
//#include <algorithm>
#include <fstream>

// In Class Code will go here 


/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* Main Driver Function 
********************************************/
int main(int argc, char** argv)
{

	// Allocate the edges as shown in the description
	std::vector< Edge<char> > edges =
	{
		{'N', 'O', 1}, {'N', 'T', 1}, {'O', 'R', 1}, 
		{'O', 'E', 1}, {'T', 'R', 1}, {'R', 'D', 1}, 
		{'E', 'D', 1} 
	};

	// construct graph
	Graph<char> charGraph(edges);

	// print adjacency list representation of graph
	printGraph(charGraph);
	
	
	// Allocate the edges as shown in the description
	std::vector< Edge<int> > edgeInts =
	{
		{0, 1, 1}, {0, 4, 1}, 
		{1, 4, 1}, {1, 2, 1}, {1, 3, 1}, 
		{2, 3, 1}, 
		{3, 4, 1} 
	};
	
	// construct graph
	Graph<int> intGraph(edgeInts);

	// print adjacency list representation of graph
	printGraph(intGraph);

	return 0;
}


