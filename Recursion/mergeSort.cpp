/**********************************************
* File: mergeSort.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Contains the main driver for mergeSort recursion 
**********************************************/
#include <iostream>
#include <cstdlib>
#include "Recurse.h"

int main(int argc, char** argv)
{
	int n, i;
	std::cout << "\nEnter the number of data element to be sorted: ";
	std::cin >> n;
 
	//int arr[n];
	//int *arr = new int[n];
	int *arr;
	arr = (int *)calloc(n, sizeof(int));
	
	std::cout << "Enter elements followed by a space: ";;
	for(i = 0; i < n; i++)
	{
		std::cin >> arr[i];
	}
 
	// Run Merge Sort
	mergeSort(arr, 0, n-1);
 
	// Printing the sorted data.
	std::cout << "\nSorted Data: ";
	
	for (i = 0; i < n; i++){
		if(i != 0){
			std::cout << " -> ";
		}
		
        std::cout<<arr[i];
	}
 
	std::cout << std::endl;
	
	// Delete the array pointer 
	delete[] arr;
	
	return 0;
}
