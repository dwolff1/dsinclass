/****************************************
 * File name: BSTNode.h  
 * Author: Matthew Morrison 
 * Contact E-mail: matt.morrison@nd.edu 
 * 
 * This file contains the class members and 
 * functions for an implementation of a 
 * Binary Search Tree Node struct.
 * *************************************/

#ifndef BSTNODE_H
#define BSTNODE_H

template<class T>
struct BSTNode
{
    T element;
    BSTNode *left;
    BSTNode *right;

    /************************************
     * Function Name: BSTNode  
     * Preconditions: const T & , BSTNode *, BSTNode *
     * Postconditions: new object   
     * Creates a new BST Node given an element and two 
     * nodes for left and right.
     * *********************************/
    BSTNode( const T & theElement, BSTNode *lt, BSTNode *rt )
      : element( theElement ), left( lt ), right( rt ) { }

    /************************************
     * Function Name: BSTNode  
     * Preconditions: T &&, BSTNode *, BSTNode *
     * Postconditions: new object   
     * Creates a new BST Node given an element and two 
     * nodes for left and right.
     * *********************************/    
    BSTNode( T && theElement, BSTNode *lt, BSTNode *rt )
      : element( std::move( theElement ) ), left( lt ), right( rt ) { }
};



#endif