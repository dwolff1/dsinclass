#ifndef SECTION01_UTILS_H
#define SECTION01_UTILS_H

#include <cstdlib>

/*************************
 *  Function Name: allocateStrMemory
 *  Preconditions: T*, size_t length 
 *  Postconditions: None
 * 
 *  Allocates memory for an array of a given length 
 * ************************/
template<class T>
void allocateMemory(T* str, const size_t length);

/*************************
 *  Function Name: deallocateStrMemory
 *  Preconditions: char **
 *  Postconditions: None
 * 
 *  Deallocates memory for an array 
 * ************************/
template<class T>
void deallocateMemory(T* arr);

/******************************
 * Function Name: copy
 * Preconditions: InputIterator first, InputIterator last, OutputIterator result
 * Postconditions: OutputIterator
 * Copies the elements in the range [first,last) into the range beginning at result.
 * ****************************/       
template<class InputIterator, class OutputIterator>
OutputIterator copy (InputIterator first, InputIterator last, OutputIterator result);

 /******************************
 * Function Name: find
 * Preconditions: InputIterator first, InputIterator last, const T& 
 * Postconditions: InputIterator
 * 
 * Iterates from the first to the last element. If a value
 * is found, it returns the iterator of that element
 * Otherwise, it will return the last iterator
 * ****************************/
template<class InputIterator, class T>
InputIterator find (InputIterator first, InputIterator last, const T& val);

/******************************
 * Function Name: swap
 * Preconditions: T2&, T2&
 * Postconditions: none
 * 
 * This swaps the members, and is called
 * by the public void swap(Array<T>& other)
 * ****************************/
template<class T2>
void swap (T2& a, T2& b);

#endif