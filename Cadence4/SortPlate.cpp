/**********************************************
 * File: SortPlate.cpp
 * Author: Dimitri Wolff
 * Email: dwolff1@nd.edu
 *
 **********************************************/
#include <iostream>
#include <vector>
#include <string>
//#include <iterator>
#include <unordered_map>
//#include <algorithm>
#include <fstream>
#include <stdio.h>
#include <ctype.h>
#include <algorithm>


int main()
{

    // Dictionary of words
    std::vector< std::string> dict={"LAUGH","FUN","SORT","CRY","MEAN","CAR","FEAR"};
    // License plates start with two or three letters, then they are followed by 5 characters, from which at most two are letters, and the rest are digits.
    std::vector<std::string> plates = {"RT123SO","RC10014","FN78U00"};
    
    // Temporary vector with only letters in liscence plates vector
    std::vector< std::vector<char> > plateChar;

    // Fill temporary vector with only letters in liscence plates vector
    for (auto idx:plates){
        std::vector< char> temp;
        for (int i=0;i<idx.length();i++){
            if ( isalpha(idx[i])!=0)
                temp.push_back(idx[i]);
        }
        plateChar.push_back(temp);
    }
    //Temporary vector for storing sorted dictionary words
    std::vector< std::vector<char> > dictTemp;
    for (auto idx:dict){
        std::vector< char> temp2;
        for (int i=0;i<idx.length();i++){
                temp2.push_back(idx[i]);
        }
        dictTemp.push_back(temp2);
    }
    // Sort letters from plates and letters from dictionary word
    for (auto idx:dictTemp)
        sort(idx.begin(),idx.end() );
    for (auto idx:plateChar)
        sort(idx.begin(),idx.end() );
    // Look for matches in the sorted words
    for (auto idx1:dictTemp){
        for (auto idx2:plateChar){
            if (idx1==idx2)
                std::cout<<"TRUE"; //print if word is found
        }
    }

    return 0;
}
