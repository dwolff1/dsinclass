/**********************************************
* File: FibHash.cpp
* Author: Dimitri Wolff 
* Email: dwolff1@nd.edu
* 
* A Fibonacci Solver Using std::map to reduce
* Recursive Calls with Memoization
*
* Compilation: g++ -g -std=c++11 -Wpedantic FibHash.cpp -o FibHash 
* Running: FibHash [int]
**********************************************/
#include <iostream>
#include <sstream>
#include <unordered_map>

int recurCalls = 0;
int recurHashCalls = 0;

/********************************************
* Function Name  : getArgv1Num
* Pre-conditions : int argc, char** argv
* Post-conditions: double
*
* Takes in int argc and char** argv, and returns a 
* double if argv[1] is a valid double  
********************************************/
double getArgv1Num(int argc, char** argv){
	if(argc != 2){
		std::cout << "Incorrect number of inputs" << std::endl;
		exit(-1);
	}
	
	// stringstream used for the conversion initialized with the contents of argv[1]
	double factNum;
	std::stringstream convert(argv[1]);

	//give the value to factNum using the characters in the string
	if ( !(convert >> factNum) ){
		std::cout << "Not a valid input" << std::endl;
		exit(-1);
	}
	
	return factNum;
	
}

/********************************************
* Function Name  : Fib
* Pre-conditions : double n
* Post-conditions: double
* 
* Finds the recursive solution to the nth
* Fibonacci number 
********************************************/
double Fib(double n){
	
	recurCalls++;
	
	if(n == 0){
		return 1;
	}
	else if(n == 1){
		return 1;
	}
	
	return Fib(n-1) + Fib(n-2);
}

// In-Class Coding Solution Goes Here 
/********************************************
* Function Name  : FibHash
* Pre-conditions : double n, std::unordered_map<double,double>* HashValues
* Post-conditions: double
* 
* Function for inclass solution 
********************************************/
double FibHash(double n, std::unordered_map<double,double>* HashValues){
	recurHashCalls++;

	//HashValues->contains(n)
	////Starting in 2020, -std=c++20
	if (HashValues->count(n) != 0){
		return(*HashValues)[n];
	}
	if (n==0  || n ==1)
		return 1;
	HashValues->insert( {n,FibHash(n-1,HashValues)+FibHash(n-2,HashValues)} );
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* Main driver function 
********************************************/
int main(int argc, char** argv){ 

	double n = getArgv1Num(argc, argv);

	std::unordered_map<double,double>HashValues;

	std::cout << "Fib(" << n << ") returns " << Fib(n);
	std::cout << " and uses " << recurCalls << " recursive calls." << std::endl;

	std::cout<<"FibHash("<<n<<") returns "<< FibHash(n, &HashValues);
	std::cout<<" and uses " << recurHashCalls << " recursive calls." << std::endl;

	std::cout << "FibHash{" << n<< ")";

	return 0;
}
